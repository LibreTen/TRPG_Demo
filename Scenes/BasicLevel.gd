extends Node2D

signal mouse_moved

const CELL_SIZE = 16
var turn_owner : Node2D
var astar := AStar2D.new()

var turn_queue = []

func _ready():
	turn_owner = $Entities/player
	$Entities/player.start_turn()
	$Cursor.start_turn()

func _input(event):
	if !turn_owner.player_controlled:
		return
	if event is InputEventMouseMotion:
		emit_signal("mouse_moved")
#	if event is InputEventMouseButton:
#		if event.button_index ==1 && event.pressed:
#			tile_selected($TileMap.world_to_map(get_global_mouse_position()))
#		if event.button_index ==2 && event.pressed:
#			turn_owner.undo()
	if event.is_action_pressed("confirm"):
		tile_selected($TileMap.world_to_map($Cursor.position))
	if event.is_action_pressed("cancel"):
		turn_owner.undo()
	if event.is_action_pressed("debug"):
		var pos =$TileMap.world_to_map(get_global_mouse_position())
		var id =astar.get_closest_point(pos,true)
		print("id: ",id,"disabled: ",astar.is_point_disabled(id))

func tile_selected(map_pos:Vector2):
	var world_pos = $TileMap.map_to_world(map_pos)#+Vector2(8,8)
	
	
	turn_owner.try_move(map_pos,world_pos)

func check_movement(pos,movement_points,entity):
	var map_pos = $TileMap.world_to_map(pos)
	var points = $TileMap.check_movement(map_pos,movement_points,entity)
	$TileMap/MoveHelper.paint(points)
	turn_owner.possible_movement = points
	return points

func draw_arrow(points):
	$TileMap/Arrows.paint_path(points)
