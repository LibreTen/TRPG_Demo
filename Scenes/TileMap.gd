extends TileMap

var astar:AStar2D

var width = get_used_rect().size.x
var height = get_used_rect().size.y
onready var entities = get_node("../Entities")

# Called when the node enters the scene tree for the first time.
func _ready():
	astar = get_parent().astar
	generate_astar()
	#$MoveHelper.paint(check_movement(Vector2(1,2),4))

func generate_astar():
	astar.clear()
	astar.reserve_space(width*height)
	for y in height:
		for x in width:
			var id = x+y*height
			var weight = 1
			var cell = get_cell(x,y)
			if cell == 1:
				continue
			astar.add_point(id,Vector2(x,y),weight)
			
			if x != 0:
				astar.connect_points(id,id-1,true)
			if y != 0:
				astar.connect_points(id,id-height,true)

func check_movement(map_pos:Vector2,movement_points,entity):
	var positions = []
	var cost_list = []
	
	var id = astar.get_closest_point(world_to_map(entity.position),true)
	print(entity.position,entity.movement_points)
	var was_disabled = astar.is_point_disabled(id)
	
	if was_disabled:
		astar.set_point_disabled(id,false)
	var start_point = astar.get_closest_point(map_pos,true)
	cost_list.resize(30+start_point)
	check_point(start_point,cost_list,movement_points)
	if was_disabled:
		astar.set_point_disabled(id,true)
	
	#print("asd: ",map_pos)
	#print(cost_list)
	for i in cost_list.size():
		if cost_list[i] != null:
			var collides = false
#			for u in entities.get_children():
#				if world_to_map(u.position).is_equal_approx(astar.get_point_position(i)):
#					collides = true
#					break
#			if !collides:
			positions.append(astar.get_point_position(i))
			if i==94:
				print(astar.is_point_disabled(i))
	
	return positions

func check_point(point,cost_list:Array,movement_points):
	cost_list[point] = movement_points
	
	for u in astar.get_point_connections(point):
		if astar.is_point_disabled(u):
			continue
		
		if u >= cost_list.size():
			cost_list.resize(u+10)
		if cost_list[u] == null || cost_list[u]<movement_points:
			var cost = astar.get_point_weight_scale(u)
			if cost <= movement_points:
				check_point(u,cost_list,movement_points-cost)
				

func update_position(entity,old_pos,new_pos):
	astar.set_point_disabled(astar.get_closest_point(world_to_map(old_pos),true),false)
	astar.set_point_disabled(astar.get_closest_point(world_to_map(new_pos),true),true)
	
