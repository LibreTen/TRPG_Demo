extends Sprite

var battle_master : Node2D
var astar :AStar2D
var map :TileMap
var cursor

var movement_points = 5

export var player_controlled = false

var delay = 0

var possible_movement : Array

var undos = []

func _ready():
	battle_master =find_parent("Battle")
	astar = battle_master.astar
	map = battle_master.get_node("TileMap")
	cursor = battle_master.get_node("Cursor")
	map.update_position(self,position,position)
	#start_turn()
	#point = astar.get_closest_point(position)

func start_turn():
	#move_points = max_move_points
	#map.check_movement(map.world_to_map(position),move_points)
	battle_master.check_movement(position,movement_points,self)
	

func try_move(map_pos:Vector2,world_pos:Vector2):
	var start_point = astar.get_closest_point(map.world_to_map(position),true)
	var end_point = astar.get_closest_point(map_pos,true)
	

	#print(map_pos,possible_movement)
	if start_point == end_point:
		pass
	elif map_pos in possible_movement:
		
		undos.append({"pos":position,"movement_points":movement_points})
		
		var spent = 0
		
		var points_pos:Array
		
		#spent = cursor.movement_points
		
		#spent = make_path(start_point,end_point,spent,points_pos)
		
		#movement_points-= spent
		movement_points = cursor.movement_points
		var path = cursor.tiles
		
		#astar.get_poi
		map.update_position(self,position,world_pos)
		position = world_pos
		#battle_master.draw_arrow(points_pos)
		battle_master.check_movement(position,movement_points,self)
		end_move()

func undo():
	if undos.empty():
		return
	var undo = undos.pop_back()
	map.update_position(self,position,undo.pos)
	position = undo.pos
	movement_points =undo.movement_points
	cursor.start_turn()

func end_move():
	cursor.start_turn()

func make_path(start_point,end_point,spent,points_pos):
	var points:Array = astar.get_id_path(start_point,end_point)
	
	for u in points:
		points_pos.append(astar.get_point_position(u))
		if start_point == u: continue
		spent += astar.get_point_weight_scale(u)
	return spent
