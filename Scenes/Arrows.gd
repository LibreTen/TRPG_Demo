extends TileMap

enum {START,STRAIGHT,TURN,END}

func paint_path(points :Array):
	clear()
	if points.size() <= 1:
		return
	for i in points.size():
		var u = points[i]
		var type
		var end_dir
		var start_dir
		
		
		if i == 0:
			type = START
		if i+1==points.size():
			type = END
		
		if type!=END:
			end_dir = points[i+1]-u
		if type != START:
			start_dir = points[i-1]-u
		
		if type== START:
			set_cell(u.x,u.y,0,false,false,false,end_dir+Vector2.ONE)
		elif type == END:
			set_cell(u.x,u.y,0,false,false,false,start_dir+Vector2(2,1))
		elif start_dir == -end_dir:
			set_cell(u.x,u.y,0,false,false,start_dir.x==0,Vector2(0,0))
		else:
			if start_dir == Vector2.UP||end_dir==Vector2.UP:
				set_cell(u.x,u.y,0,start_dir+end_dir==-Vector2.ONE,false,true,Vector2(0,3))
			else:
				set_cell(u.x,u.y,0,start_dir+end_dir==Vector2.ONE,false,false,Vector2(0,3))
			
		#set_cell(u.x,u.y,0,false,false,false,Vector2.ZERO)
