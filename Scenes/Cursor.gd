extends AnimatedSprite

enum  {UP,DOWN,LEFT,RIGHT}

const TIMER_FIRST_DELAY = 0.25

var battle_master
var map : TileMap
var arrows : TileMap
var astar : AStar2D
var tiles = []
var movement_points :int
var possible_movement :Array
var holding_key

func _ready():
	battle_master = find_parent("Battle")
	map = battle_master.get_node("TileMap")
	arrows = battle_master.get_node("TileMap/Arrows")
	astar = map.astar
	battle_master.connect("mouse_moved",self,"mouse_moved")

func _input(event):
	var updated = false
	
	if event.is_action_pressed("ui_up"):
		move_cursor(UP)
		holding_key = UP
		$Timer.start(TIMER_FIRST_DELAY)
	elif event.is_action_pressed("ui_down"):
		move_cursor(DOWN)
		holding_key = DOWN
		$Timer.start(TIMER_FIRST_DELAY)
	elif event.is_action_pressed("ui_left"):
		move_cursor(LEFT)
		holding_key = LEFT
		$Timer.start(TIMER_FIRST_DELAY)
	elif event.is_action_pressed("ui_right"):
		move_cursor(RIGHT)
		holding_key = RIGHT
		$Timer.start(TIMER_FIRST_DELAY)
	elif (event.is_action_released("ui_up") && holding_key == UP) || (event.is_action_released("ui_down") && holding_key == DOWN)||(event.is_action_released("ui_left") && holding_key == LEFT) ||(event.is_action_released("ui_right") && holding_key == RIGHT):
		holding_key = -1
		$Timer.stop()
	


func move_cursor(d):
	match d:
		UP:
			position += Vector2(0,-16)
		DOWN:
			position += Vector2(0,16)
		LEFT:
			position += Vector2(-16,0)
		RIGHT:
			position += Vector2(16,0)
	var map_pos = map.world_to_map(position)
	select_tile(map_pos)

func start_turn():
	tiles.clear()
	arrows.paint_path(tiles)
	tiles.append(map.world_to_map(battle_master.turn_owner.position))
	movement_points = battle_master.turn_owner.movement_points
	possible_movement = battle_master.check_movement(battle_master.turn_owner.position,movement_points,battle_master.turn_owner)
	select_tile(map.world_to_map(position))

func mouse_moved():
	var map_pos = map.world_to_map(get_global_mouse_position())
	var world_pos = map.map_to_world(map_pos)
	
	if position != world_pos:
		position = world_pos
		select_tile(map_pos)


func select_tile(map_pos:Vector2):
	var id = astar.get_closest_point(map_pos,true)
	#TODO check if disabled
	var cost = astar.get_point_weight_scale(id)
	
	var start_id = astar.get_closest_point(tiles.front(),true)
	#var id = astar.get_closest_point(world_to_map(entity.position),true)
	#print(entity.position,entity.movement_points)
	var was_disabled = astar.is_point_disabled(start_id)
	if was_disabled:
		astar.set_point_disabled(start_id,false)
	
	var previous_id = astar.get_closest_point(tiles.back())
	var previous_connections = astar.get_point_connections(previous_id)
	if (id in previous_connections && map_pos.distance_to(tiles.back()) == 1
		&& cost <= movement_points && !(map_pos in tiles)): #Advancing a tile
			if !astar.is_point_disabled(astar.get_closest_point(map_pos,true)):
				movement_points -= cost
				tiles.append(map_pos)
				arrows.paint_path(tiles)
	elif map_pos == tiles.back():
		pass
	elif map_pos in tiles:
		var i = tiles.find(map_pos)
		for u in tiles.slice(i+1,-1):#tiles.size()+1):
			movement_points+=astar.get_point_weight_scale(astar.get_closest_point(u))
		tiles = tiles.slice(0,i)
		arrows.paint_path(tiles)
	elif map_pos in possible_movement: #Recreate path
		for u in tiles.slice(1,-1):
			movement_points+=astar.get_point_weight_scale(astar.get_closest_point(u))
		var path :Array = astar.get_id_path(start_id,id)
		path.pop_front()
		var path_pos = [tiles.front()]
		for u in path:
			path_pos.append(astar.get_point_position(u))
			movement_points-=astar.get_point_weight_scale(u)
		tiles = path_pos
		arrows.paint_path(tiles)
	
	if was_disabled:
		astar.set_point_disabled(start_id,true)


func _on_Timer_timeout():
	#$Timer.wait_time = 0.1
	$Timer.start(0.05)
	move_cursor(holding_key)
